FROM ubuntu:bionic
MAINTAINER AXE Labs "service@axelabs.com"

WORKDIR /home/

RUN apt-get update && apt-get install -y psmisc bc curl jq
ADD *.sh /home/
RUN ln jtp.sh rewards

ENTRYPOINT ["/home/rewards"]
