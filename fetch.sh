#!/bin/bash

# This script accept a URL paramter and parses rewards and URL's out of its JSON payload

[[ "$1" ]] || exit 1  

URLDATA=${URLDATA:-/dev/shm/jtp.urls}
REWARDS=${REWARDS:-/dev/shm/jtp.rewards}
CACHING=${CACHING:-/dev/shm/jtp}
URLHASH=`md5sum <(echo $1)|awk '{print $1}'`

# If URL has not been previously cached, get it and store it
mkdir -p $CACHING
[[ ! -s $CACHING/$URLHASH ]] && curl -s "$1" -o $CACHING/$URLHASH

# Parse data from cache and store in pertinent files
cat $CACHING/$URLHASH | jq '.reward, .children[]' -r 2> /dev/null | tee \
  >(grep http >> $URLDATA) \
  >(grep ^[-0-9]>>$REWARDS) > /dev/null
