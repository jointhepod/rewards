#!/bin/bash

# This script accepts a URL as a paramter and passes it to a data fetcher.
# A background watcher monitors for more URL's obtained by fetching.
# When no more new data is observed, the data summary is generated.

[[ "$1" ]] || {
  echo "Usage:
  `basename $0` URL"
  exit 1
}

export URLDATA=/dev/shm/jtp.urls
export REWARDS=/dev/shm/jtp.rewards
export CACHING=/dev/shm/jtp

# Launch watcher for processing derived URLs
./watcher.sh &

# Start fetching data
./fetch.sh $1 &

# If no more new URL's are coming in, stop watch
while :;do
  LR=`stat -c%Y $URLDATA 2> /dev/null`
  [[ "$LR" ]] && [[ "$(( `date +%s`-$LR  ))" -gt 15 ]] && break
  sleep 1
done

# Generate data summary
[[ "$DEBUG" ]] && cat $REWARDS
echo -n "Rewards total: "
bc <<< "scale=2
  `cat $REWARDS | tr '\n' '+' | sed 's/+$//'`"
echo "From $((`cat $URLDATA|wc -l`+1)) URL(s)."

# Tidy up
rm -rf $URLDATA $REWARDS $CACHEES
XARGS=`pgrep --ns $$ xargs`
disown
killall -n $$ tail
wait
