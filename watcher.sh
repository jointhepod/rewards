#!/bin/bash

# This script looks for URL's in a file and spawns a fetch request for new ones

URLDATA=${URLDATA:-/dev/shm/jtp.urls}
MAXPROC=${MAXPROC:-15}

[[ "$DEBUG" ]] && DEBUG="--verbose"

touch $URLDATA
tail -f $URLDATA | xargs -P$MAXPROC -rn1 $DEBUG ./fetch.sh
