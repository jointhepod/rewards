# rewards

This repository contains shell code which parses data from jointhepod.com.

## Prerequisites

The code was tested on a Ubuntu Bionic docker container witha a few extra packages.

Setup:
```
git clone https://axelabs@bitbucket.org/jointhepod/rewards.git && cd rewards
docker build -t rewards .
alias rewards="docker run -t rewards"
```

## Usage
```
rewards URL
```
Example:
```
rewards http://devops.jointhepod.com/f
Rewards total: 5
From 2 URL(s).
```
